

class Employee {
    constructor(name = 'Kate', age = 100, salary = 200 ) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }


    get name() {
        return this._name 
    }
    set name(newName) {
        this._name = newName
    }


    get age() {
        return this._age 
    }
    set age(newAge) {
        this._age = newAge
    }


    get salary() {
        return this._salary
    }
    set salary(newSalary) {
        this._salary = newSalary
    }
      getEmploy (){
          console.log( this.name, this.age, this.salary);
      }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this._lang = lang
    }

   
    get lang() {
        return this._lang
    }
    set lang(newSalary) {
        this._lang = newSalary
    }


    get salary() {
       return this._salary*3
    }
    set salary(newSalary) {
        this._salary = newSalary
    }


    getInfo (){ 
        return console.log(`Наш работник и сослуживец по имени ${this.name}, дожив до своих ${this.age} лет сумел достичь ,в некотром смысле, завидного дохода  ${this.salary} монеток в год и с успехом освоил ${this.lang} язык - язык своих предков.` );}

}

const Programmer1 = new Programmer ("Василий", 35, 1000, 'английский')
const Programmer2 = new Programmer ("Петр", 50, 1500, 'арабский')
const Programmer3 = new Programmer ("Софья", 18, 2000, 'хинди')


Programmer1.getInfo()
Programmer2.getInfo()
Programmer3.getInfo()
