const body = document.querySelector('.container');
const urlUsers = 'https://ajax.test-danit.com/api/json/users';
const urlPosts = 'https://ajax.test-danit.com/api/json/posts';

class Card {
  /**
   * 
   * @param {function} removeFunction 
   */
  constructor(removeFunction, name, email, title, body, postId) {
    this.postFirstName = name;
    this.postEmail = email;
    this.postTitle = title;
    this.postBody = body;
    this.postId = postId;
    this.removeFunction = removeFunction;
    this.card = document.createElement('div');
    this.contentContainer = document.createElement('div');
    this.removeBtn = document.createElement('button');
    this.contacts = document.createElement('div');
    this.lastName = document.createElement('p');
    this.name = document.createElement('p');
    this.mail = document.createElement('p');
    this.post = document.createElement('div');
    this.header = document.createElement('h1');
    this.img = document.createElement('img');
    this.twit = document.createElement('p');
  }


  createElements() {
    this.card.classList.add('card');
    this.contentContainer.classList.add('contantContainer');
    this.removeBtn.classList.add('removeBtn');
    this.contacts.classList.add('contacts');
    this.lastName.classList.add('lastName');
    this.name.classList.add('name');
    this.mail.classList.add('mail');
    this.post.classList.add('post');
    this.header.classList.add('header');
    this.img.classList.add('img');
    this.twit.classList.add('twit');

    this.name.innerHTML = this.postFirstName;
    this.mail.innerHTML = this.postEmail;
    this.twit.innerHTML = this.postBody;
    this.header.innerHTML = this.postTitle;
    this.removeBtn.innerHTML = `Remove post # ${this.postId}`;

    this.post.append(this.header);
    this.post.append(this.img);
    this.post.append(this.twit);

    this.contacts.append(this.lastName);
    this.contacts.append(this.name);
    this.contacts.append(this.mail);

    this.contentContainer.append(this.contacts);
    this.contentContainer.append(this.post);

    this.card.append(this.removeBtn);
    this.card.append(this.contentContainer);

    this.removeBtn.addEventListener('click', this.removeFunction.bind(this));
  }
  /**
   * 
   * @param {string} selector 
   */

  render(selector = "body") {
    this.createElements();
    document.querySelector(selector).append(this.card);
  }
}

class PostCard {
  constructor(postUrl, userUrl) {
    this.postUrl = postUrl;
    this.userUrl = userUrl;
  }

  async getData() {
    await axios.get(this.postUrl).then(({
      data
    }) => {
      this.posts = data
    })
    await axios.get(this.userUrl).then(({
      data
    }) => {
      this.users = data
    })
    this.posts.forEach(post => {

      const filtredUser = this.users.filter((user) => {
        if (post.userId === user.id) {

          return true
        } else {

          return false
        }

      })

      const user = filtredUser[0]

      const card = new Card(removeFunction, user.name, user.email, post.title, post.body, post.id)
      card.render()
    })

  }
}
const removeFunction = function () {
  axios.delete(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
    id: this.postId
  }).then(({
    status
  }) => {
   
    if (status === 200) {

      this.card.remove()
    }
  })
  }
const postCard = new PostCard(urlPosts, urlUsers);
postCard.getData()

