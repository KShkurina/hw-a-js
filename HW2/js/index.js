const books = [{
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];


class TrueBooks  {

    constructor(selector, arr) {
      this.selector = selector;
      this.arr = arr;
    }
  
    showbooks() {
        const container = document.querySelector(this.selector)
        const ul = document.createElement('ul')
        
        this.arr.forEach(({name,author,price}) =>{
            
            try {
                if (!name) {
                    throw new Error("нет названия")
                };
                if (!author) {
                    throw new Error("нет автора")
                };
                if (!price) {
                    throw new Error("нет цены")
                };
                let li = document.createElement('li');
               
                li.innerHTML = `" ${name} " <br>    Автор ${author}<br>    Цена ${price}`
                
                 container.insertAdjacentElement('beforeend', ul);
               
                ul.insertAdjacentElement('beforeend', li);
    
            } catch (error) {
                console.log(error);
            }
        })
        // })
      
    }
  
}
  
  let user = new TrueBooks('#root',books);
  user.showbooks()
 