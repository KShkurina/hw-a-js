fetch('https://ajax.test-danit.com/api/swapi/films').then(response => response.json()).then((data) => {
    console.log(data);
    data.forEach(({
        episodeId,
        name,
        openingCrawl
    }) => {
        const films = document.createElement('div');
        films.classList.add('div');
        films.innerHTML =
            `<div id='films'>
            <div class="wrapper">
            <div class="episode">EPISODE ${episodeId}</div>
            <div class="name">"${name}"</div></div>
            
            <div class="summary">${openingCrawl}</div>
            </div>`;
        const img = document.createElement('img')
        img.src = "b4a94b786bffe582cf5824950a8596f4.gif";
        const names = document.querySelectorAll('.wrapper');
        names.forEach((e) => {
            e.append(img);
        });
        document.body.append(films);
    });
    const nameDiv = document.querySelectorAll('.wrapper');
    for (let i = 0; i < nameDiv.length; i++) {
        let ulForChar = document.createElement('ul');
        ulForChar.classList.add('list');
        let filmStars = [...data[i].characters]
        filmStars.forEach((element) => {
            fetch(element).then(response => response.json()).then((data) => {
                let liForChar = document.createElement('li');
                liForChar.innerHTML =`${data.name}`;
                ulForChar.insertAdjacentElement('beforeend', liForChar);
                nameDiv[i].insertAdjacentElement('afterend', ulForChar);
                document.querySelector('img').remove();
            });
        });
    }
});