const button = document.querySelector('.button');
const container = document.querySelector('body');

let ip
button.addEventListener('click', async () => {
    await axios.get('https://api.ipify.org/?format=json')
        .then(({
            data
        }) => {
            ip = data.ip;

        });
    console.log(ip);
    await axios.get(`http://ip-api.com/json/${ip}?fields=continent,country,region,regionName,city,dist`)
        .then((response) => {
            let newinfo = response.data;
            newInfoElements = Object.values(newinfo)
            console.log(newinfo);
            newInfoElements.forEach(element => {
                console.log(element);
                let info = document.createElement('p');
                info.innerHTML = `${element}`;
                container.insertAdjacentElement('beforeend', info);
            });
        });
})